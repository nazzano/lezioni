//ES8
fetch()
//* (guardare async giu)
.then(response => response.json())
.then(val => console.log(val))

// Una funzione asincrona inizia la sua firma con la chiave "async"
// all'interno delle funzioni asincrone posso utilizzare la parola chiave "await"
// Await si usa solo se sta all'interno alle funzioni con async
async function myAsyncFunc() {
    //* (un altro modo per scrivere then)
    // Await serve a registrarsi in ascolto ad una Promise
    const response = await fetch();
    const val = await response.json();
}

    // Eè identico a:
    // new Promise((resolve, reject) => {
    // fetch(
    //    .then(response.json())
    //    .then(vale => resolve(val));
    //)
    //});


    // Le funzioni asincrone sono trattate come Promise, tanto che posso agganciarmici
    //con "then". La chiave "async" è quindi un wrapper di funzioni all'interno di Promise
    // myAsyncFunc().then

    