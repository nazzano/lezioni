// Map, a differenza degli oggetti Javascript (istanziati con {}) permette di creare
// delle coppie di dati (chiave - valore) dove la chiave può essere qualsiasi tipo di dato
//
// Ad es:
//  se in un oggetto la chiave true viene convertita a stringa....
//  {true: 'valore'} => true diventa 'true' (la stringa true)
// Con Map il valore true (booleano) rimarrà inalterato
//  Map() -> true => 'valore'
// PEr accedere a 'valore' dovremo passare il valore booleano true e non la stringa 'true'
//
// Map è un oggetto istanza e come tale deve essere istanziato:
const m = new Map();
// Set permette di creare una associazione chiave -> valore
m.set(1, 'Valore associato alla chiave 1 numerica');
m.set('1', 'Valore associato alla chiave 1 stringa');
// Get permette di recuperare il valore associato ad una chiave
const val1number = m.get(1);
const val1string = m.get('1');

// Keys permette di recuperare un array con tutte le chiavi presenti nel Map
m.keys();

// Delete rimuove una coppia chiave -> valore a partire dalla chiave fornita
m.delete('1');

// Clear elimina tutte le coppie chiave -> valore
m.clear();

// Quando possiamo usare Map?
// Esempio di cache con Map
const cache = new Map();

function fnMoltoComplessa(v) {
    return v * 2;
}
// Una decorator function serve ad inglobare un'altra funzione e ad esterne le funzionalità
// Ad esempio, cacheDecorator prenderà una funzione e le avvolgerà intorno un meccanismo di cache
// 
// I decorator si applicano invocando il decorator , passandogli la funzione da "migliorare"
// e recuperando la nova funzione che il decorator ci restituisce
function cacheDecorator(fn) {
    const cache = new Map();

    return v => {
        // Has permette di stabilire se una chiave esiste alll'interno di Map
        // Restituisce true / false
        if (cache.has(v)) {
            return cache.get(v);
        }
    
        const res = fn(v);
        cache.set(v, res);
    
        return res;
    }
}

// Applico il decorator della cache passando ad esso la funzione da "avvolgere"
const nuovaFnMolotComplessa = cacheDecorator(fnMoltoComplessa);

// Per invocare la fnMoltocompleass con il meccanisco di cache, uso la funzione
// che mi ha restituito il decorator
nuovaFnMolotComplessa(4);


class myMap {
    #_keys = new Set();
    #_values = [];

    costructor() {

    }

    set(key, value) {
        if (this.#_keys.has(key)) {
            return;
        }

        this.#_keys.add(key);
        this.#_values.push(value);
    }
}