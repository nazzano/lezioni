//function User(id, email) {
//    this.id = id;
//    this.email = email;
//}
//
//User.prototype.sayHi = () => {
//
//};
//
//const u = new User();
//u.sayHy();

// Class è la chiave per definire una classe. Serve a sostituire la definzione dei costruttori ES5 
//(orintati alle funzioni) a favore di una sintassi universalmente riconosciuta dai linguaggi orientati
// alla Programmazione ad Oggetti 
class User {
    // Metodo che verrà invocato facendo "new USer" 
    constructor(id, email) {
        this.email = email;
        this.id = id;
    }

    // Invece di afire su prototype, posso definire i metodi direttamente all'interno della classe:
    sayHi() {

    }
}

// Per estendere una superclasse, non agisco più sul prototype ma uso la chiave extends:
// Admin (la sottoclasse) prenderà tutte le proprietà ed i metodi della superclasse User
class Admin extends User {
    constructor(id, email) {
        // con super invoco il costructor di User
        super(id, email);
    }

    showId() {
        // Con super accedo a proprietà e metodi della superclasse
        // Sto invocando sayHi dichiarato nella superclasse User
        super.sayHi();
        alert(`Sono l'admin con id ${super.id}`);
    }
}

new Admin(1, 'email@dominio.com');