// Set è un oggetto istanza che serve a contenere collezioni di informazioni
// a differenza degli array, in cui gli elementi possono ripetersi, Set non 
// consente la ripetizione del medesimo elelnto all'interno dellla collezione
// [1, 2, 1, 2] => OK in un array
// NON consentito in un Set

const s = new Set();
s.add(1);
s.add('1');
// s.add(1); // NON posso ripetere l'elemento numerico 1

// has verifica la presenza di un elemento nella collezione (restiruisce true o false)
s.has(1);

// Delete rimuove un elemento dalla collezione
s.delete(1);

//clear rimuove gli elementi dalla collezione
s.clear();



class mySet {

    //#_ => Proprietà privata
    #_values = [ ];
    constructor() {
    }

    add(value) {
        // L'elemento contenuto in "value" esiste già
        // non devo aggiungerlo nuovamente
        if (this.#_values.includes(value)) {
            return;
        }

        this.#_values.push(value);
    }

    clear() {
        this.#_values = [];
    }

    delete(value) {
        const pos = this.#_values.indexOf(value);

        if (pos === -1) {
            return;
        }

        this.#_values.splice(pos, 1);
    }

    has(value) {
        return this.#_values.includes(value);
    }
}

const ms = new mySet();

console.log(ms);