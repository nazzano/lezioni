// Con le promise non si possono eseguire due funzioni allo stesso momento
//pending è quando la promise inizia la sua esecuzione e deve andare solo una su questi due:
// resolved
//rejected (errore)

const p = new Promise((reosolve, rejected) => {
    return resolved('valore da restuire');
});

p.then(
    successo => console.log(successo),
    errore => console.log(errore)
);

fetch().then(
    response => {},
    error => console.log('timeout')
);