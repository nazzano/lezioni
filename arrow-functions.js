var fn = function () {

}

// Si chiama Arrow Function poichè simbologia => ricorda una freccia

// => separa la parte dichiarativa (la firma) dal corpo
//
const f = () => {};

// Quando ho solo 1 parametro formale, posso omettere le parentesi tonde. Ad es: 
// a => {}
// Non è necessario quindi scrivere :
// (a) => {}

// Quando ho piu di un parametro formale, sono costretto a reintrodurre leparentesi tonde. Ad es:
// (a. b) => {}

// Quando il corpo della funzione è costituito da UNA SOLA ISTRUZIONE, le parentesi graffe , il risultato
// della singola espressione viene restituito automaticamente dalla singola 