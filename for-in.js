// for-in
const o = {
    //k = chiave
    k1: "valore",
    k2: 10,
    chiappa2: {},
  };
  //ES6
  //const key conterrà la chiave che di volta in volta viene analizzata dal for-in
  for (const key in o) {
    //o.key = accedo alla proprioetà "key" nell'oggetto "o" NON è corretta perchè la costante key non viene
    //interpellata
  
    // Accedo alla chiave presente in "key" (la prima volta conterrà "k1", la seocnda "k2"...)
    // all'interno dell'oggetto
    console.log(key, o[key]);
  }
  
  //ES5
  Object.keys(o).forEach(function (key) {
    console.log(key, o[key]);
  });
  
  //In (operator)
  const o2 = {
    k1: undefined,
    k2: 10,
  };
  
  // Nella console non leggiamo k1 perche è undefined e in booleano è false
  // Mentre k2, booleano, è true
  //if (o2.k1) {
  //console.log("k1 esiste");
  //}
  
  //if (o2.k2) {
  //console.log("k2 esiste");
  //}
  
  // Anche una chiave che non esiste in un oggetto restituisce "undefined", come spesso accade in JS
  //quando si tenta di accedere a degli elementi inesistenti
  
  if ("k1" in o) {
    console.log("k1 esiste");
  }
  
  if ("k2" in o) {
    console.log("k2 esiste");
  }
  