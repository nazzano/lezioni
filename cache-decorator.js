const cacheDecorator = fn => {
    const cache = new Map();

    // La funzione restituita dal decorator sfrutta il REst Operator per poter essere invocata
    // con un numeri arbitrario di argomenti
    return (...params) => {
        // Join crea una stringa contenete tutti gli elementi di un array, separati
        // attraverso il separatore specificato
        let k = params.map(p => `${typeof(p)}:${p}`).join(',');

        if (cache.has(k)) {
            return cache.get(k);
        }
    
        const res = fn(...params);
        cache.set(k, res);
    
        return res;
    }
}

const funzioneLenta = (v1, v2) => {
    // Ci metto un sacco di tempo... avrei proprio bisogno di una cache
    return v1 * v2;
}

const funzioneDecorata = cacheDecorator(funzioneLenta);
// params = ['1', '10']
// map = ['string:1', 'string:10']
//join 'string:1,string:10'
funzioneDecorata('1', '10'); // string:1,string:10 // 1,10
funzioneDecorata(1, '10'); // number:1,string:10 // 1,10